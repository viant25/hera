package com.example.user.hera;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {

    private EditText input_email;
    private Button btnRestPass;
    private TextView btnBeck;
    //TO DO: DELETE THIS TEMP BUTTON AFTER PICUTRES ARE OK
    private Button btnTemp;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        input_email = (EditText) findViewById(R.id.email_input);
        btnRestPass = (Button) findViewById(R.id.restBtn);
        btnBeck = (TextView) findViewById(R.id.btnBack);

        //delete after
        btnTemp = (Button)findViewById(R.id.button3);

        auth = FirebaseAuth.getInstance();

        btnTemp.setOnClickListener(this);

        btnBeck.setOnClickListener(this);
        btnRestPass.setOnClickListener(this);
         /*   @Override
            public void onClick(View v) {
                resetPassword(input_email.getText().toString());
            }
        });*/
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnBack)
        {
            startActivity(new Intent(ForgotPassword.this,MainActivity.class));
            finish();
        }
        else if(v.getId() == R.id.restBtn)
        {
            resetPassword(input_email.getText().toString());
        }
        else if(v.getId() == R.id.button3){
            startActivity(new Intent(ForgotPassword.this,Agenda.class));

        }



    }

    private void resetPassword(final String email) {

        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful())
                        {
                            Toast.makeText(ForgotPassword.this,"We have sent password to email: "+email,Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(ForgotPassword.this,"Failed to send password",Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }
}
