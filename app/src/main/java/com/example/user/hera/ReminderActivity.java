package com.example.user.hera;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class ReminderActivity extends AppCompatActivity implements
        TimePickerDialog.OnTimeSetListener,DatePickerDialog.OnDateSetListener{

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private Toolbar toolbar;

    FloatingActionButton btn_FAB;
    int day,month,year,hour,min;
    int dayFinal,monthFinal,yearFinal,hourFinal,minFinal;
    EditText dialog_time,dialog_date;
    TextView dialog_tvDate , dialog_tvTime;
    Button dialog_reminder;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<Reminders> reminderList = new ArrayList<>();


    SharedPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);

        toolbar = (Toolbar) findViewById(R.id.include_reminder);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Reminder");

        mDrawerLayout = (DrawerLayout) findViewById(R.id.reminder_drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.reminder_navView);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case(R.id.my_hera): {
                        startActivity(new Intent(ReminderActivity.this, StartActivity.class));
                        break;
                    }
                    case(R.id.weekly_tests): {
                        startActivity(new Intent(ReminderActivity.this, WeeklyTests.class));
                        break;
                    }
                    case(R.id.download_images): {
                        startActivity(new Intent(ReminderActivity.this, Download.class));
                        break;
                    }
                    case(R.id.forbidden_food): {
                        startActivity(new Intent(ReminderActivity.this, forbiddenFood.class));
                        break;
                    }
                    case(R.id.menu_reminder): {
                        startActivity(new Intent(ReminderActivity.this, ReminderActivity.class));
                        break;
                    }
                    case (R.id.upload_pics):
                        startActivity(new Intent(ReminderActivity.this, upload_pictures_types.class));
                        break;
                }
                return true;

            }
        });

        preference = new SharedPreference() ;

        //  recyclerView = (RecyclerView) findViewById(R.id.recyvler_view);

        recyclerView = (RecyclerView) findViewById(R.id.reminder_recyvler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        reminderList = new ArrayList<>();








        btn_FAB = (FloatingActionButton) findViewById(R.id.FAB);
        btn_FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder mBuilder= new AlertDialog.Builder(ReminderActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.dialog,null);
                dialog_time = (EditText) mView.findViewById(R.id.dialog_time);
                dialog_date = (EditText) mView.findViewById(R.id.dialog_date);
                dialog_reminder = (Button) mView.findViewById(R.id.btn_reminder);

                dialog_tvDate = (TextView) mView.findViewById(R.id.dialog_tvDate);
                dialog_tvTime = (TextView) mView.findViewById(R.id.dialog_tvTime);

                dialog_tvDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Calendar c = Calendar.getInstance();
                        year = c.get(Calendar.YEAR);
                        month = c.get(Calendar.MONTH);
                        day = c.get(Calendar.DAY_OF_YEAR);

                        DatePickerDialog datePickerDialog = new DatePickerDialog(ReminderActivity.this,ReminderActivity.this,year,month,day);
                        datePickerDialog.show();

                    }
                });



                dialog_tvTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Calendar c = Calendar.getInstance();
                        hour = c.get(Calendar.HOUR_OF_DAY);
                        min = c.get(Calendar.MINUTE);

                        TimePickerDialog timePickerDialog = new TimePickerDialog(ReminderActivity.this,ReminderActivity.this,
                                hour,min, DateFormat.is24HourFormat(ReminderActivity.this));
                        timePickerDialog.show();
                    }
                });



                dialog_reminder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent calIntent = new Intent(Intent.ACTION_INSERT);
                        calIntent.setType("vnd.android.cursor.item/event");
                        calIntent.putExtra(CalendarContract.Events.TITLE, "doctor's appointment");

                        GregorianCalendar ScalDate = new GregorianCalendar(yearFinal, monthFinal, dayFinal,hourFinal,minFinal);
                        GregorianCalendar EcalDate = new GregorianCalendar(yearFinal, monthFinal, dayFinal,hourFinal,minFinal);
                        calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                                ScalDate.getTimeInMillis());
                        calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                                EcalDate.getTimeInMillis());
                        String date = Integer.toString(dayFinal) + "." + Integer.toString(monthFinal) + "." + Integer.toString(yearFinal);
                        String time = Integer.toString(hourFinal) + ":" + Integer.toString(minFinal);



                        // Creating Reminder


                        Reminders reminders = new Reminders("doctor's appointment",time,date);

                        reminderList.add(reminders);

                        preference.addFavorite(ReminderActivity.this,reminders);

                        adapter.notifyDataSetChanged();

                        startActivity(calIntent);
                    }
                });

                mBuilder.setView(mView);
                AlertDialog dialog = mBuilder.create();
                dialog.show();

            }
        });

        File f = new File("/data/data/" + getPackageName() +  "/shared_prefs/NKDROID_APP.xml");


        if(f.exists())
        {
            List<Reminders> reList = new ArrayList<>();
            reList = preference.loadFavorites(ReminderActivity.this);
            reminderList.addAll(reList);
        }

        /*List<Reminders> reList = new ArrayList<>();
        reList = preference.loadFavorites(MainActivity.this);
        reminderList.addAll(reList);*/

        adapter = new ReminderAdapter(reminderList,this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDateSet(DatePicker view, int i, int i1, int i2) {

        yearFinal = i;
        monthFinal = i1;
        dayFinal = i2;

        dialog_date.setText(dayFinal + "." + monthFinal + "." + yearFinal);
    }

    @Override
    public void onTimeSet(TimePicker view, int i, int i1) {

        hourFinal = i;
        minFinal = i1;

        dialog_time.setText(hourFinal + ":" + minFinal);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
