package com.example.user.hera;

/**
 * Created by Guy on 15/04/2017.
 */

import android.support.annotation.NonNull;

import com.google.gson.JsonElement;

import java.util.Map;
import java.util.Random;
import ai.api.model.AIResponse;
import ai.api.model.ResponseMessage;
import ai.api.model.Result;

public class ApiAiAnalyzer extends StartActivity {

    static int missunderstoodQuerys;
    public String videoNameFinder(final Result result) {
         String videoName="";
        switch (result.getMetadata().getIntentName()) {
            case "boy names":
                //return here you go video name string.
                break;
            case "stuffComplains":
                //return here you go video name string.
                String ansers[]={"emosupport1","emosupport2","emosupport3"};
                videoName=new String(randomizedAnswerSelect(ansers));

                break;
            case "acquaintanceComplains":
                videoName=new String(peopleComplainsResponse(result));
                break;
            case "goodPregnancyRemarks":
                //return here you go video name string.
                break;
            
            case "exercise practice":
                videoName=new String(exercisePracticeAnswer(result)) ;
            break;
            case "exercise start":
                //return recommended sports videos
                break;
            case "food questions":
                videoName=new String(foodQuestionAnser(result));
            break;
            case "Normal?":
                videoName=new String (normalAnswer(result));
            break;
            case "feelings":
                String parameterString=null;
                for (final Map.Entry<String, JsonElement> entry : result.getParameters().entrySet()) {
                    parameterString = entry.getValue().getAsString();}
                    switch (parameterString) {
                        case "good":
                            videoName =new String(positiveFeelingRespones(result)) ;
                            break;
                        case "bad":
                            videoName=new String(negatoveFeelingRespones(result));
                        break;
                    }
                   return videoName;

            case "doctor results":
                //return here you go video name.
            case "Default Fallback Intent":
                videoName=new String (unknownQuestion(result));
            break;
            case "test query":
                videoName=new String(testQueryAnalizer(result));
                break;
            case "babySize":
               videoName= "ppfw1";
                break;

        }
        return videoName;
    }

    private String peopleComplainsResponse(Result result) {
        return new String("peoplecomments1");
    }

    private String negatoveFeelingRespones(Result result) {
        return "emosupport2";
    }

    private String positiveFeelingRespones(Result result) {
        return new String("itswonderful");
    }

    private String unknownQuestion(Result result) {
        return "unknwon1";
    }


    public static String testQueryAnalizer(final Result result) {
        String parameterString=null;
        for (final Map.Entry<String, JsonElement> entry : result.getParameters().entrySet()) {
            parameterString = entry.getValue().getAsString();
        }
        return parameterString+" exam";
    }

    private String normalAnswer(Result result) {
        String[] videoNames={"breath","constipation","cramping","faitgue","gumbleed","heartburn","leaks",
                             "nausea","nosebleed","pain","swelling","urination"};
        String[] unknownAns={"normalunknown1","normalunknown2","normalunknown3"};
        String parameterString=null;
        for (final Map.Entry<String, JsonElement> entry : result.getParameters().entrySet()) {
            parameterString = entry.getValue().getAsString();
        }
        String answer="";
        if (parameterString==null)
            answer=new String(randomizedAnswerSelect(unknownAns));
        else{
            switch (parameterString){
                case "breathing":answer=new String(videoNames[0]);break;
                case "constipated":answer=new String(videoNames[1]);break;
                case "crampping":answer=new String(videoNames[2]);break;
                case "fatigue":answer=new String(videoNames[3]);break;
                case "gumsbleeding":answer=new String(videoNames[4]);break;
                case "heartburn":answer=new String(videoNames[5]);break;
                case "leak urination":answer=new String(videoNames[6]);break;
                case "nausea":answer=new String(videoNames[7]);break;
                case "nosebleeding":answer=new String(videoNames[8]);break;
                case "pain":answer=new String(videoNames[9]);break;
                case "swelling":answer=new String(videoNames[10]);break;
                case "urination":answer=new String(videoNames[11]);break;

            }}
        return  answer;
    }

    private String exercisePracticeAnswer(Result result) {
        String[] videoNames = {"unsafeDueFalling", "unsafeDueTrauma", "chess","unknown"};//diffrent / unrecommended answers
        String[] recommended = {"good1", "good2", "good3", "good4"};//good sports activies answers
        String answer="";
        final Map.Entry<String, JsonElement> entry = (Map.Entry<String, JsonElement>) result.getParameters().entrySet();
        if (new String("recommended").equals(entry.getValue().toString())) {
            return randomizedAnswerSelect(recommended);
        } else {
            switch (entry.getValue().toString()){
                case "":
                    answer=new String(videoNames[3]);
                    break;
                case"unsafeDueFalling":
                    answer=new String(videoNames[0]);
                    break;
                case "unsafeDueTrauma":
                    answer=new String(videoNames[1]);
                    break;
                case "chess":
                    answer=new String(videoNames[2]);
                    break;
            }
            return answer;
        }

    }
    String foodQuestionAnser(Result result){
        String [] videoNames={"alocohol","genericbad","largefish","pickeledfish","rawfish","shellfish","rawmeat","sauseges","juice","icecream"};
                /*             alcohol  cheese(generic) largeFish  pickeledFish   rawFish shellfish  rawMeat*/
        String[] good={"sure","delicious","yes","youcan"};
        String parameterString=null;
        for (final Map.Entry<String, JsonElement> entry : result.getParameters().entrySet()) {
            parameterString = entry.getValue().getAsString();
        }
        String answer="";
        if (parameterString==null)
            answer=new String(randomizedAnswerSelect(good));
        else{
        switch (parameterString){

            case"raw fish":
                answer=new String(videoNames[4]);
                break;
            case "shellfish":
                answer=new String(videoNames[5]);
                break;
            case "large fish":
                answer=new String(videoNames[2]);
                break;
            case "pickled fish":
                answer=new String(videoNames[3]);
                break;
            case "raw meat":
                answer=new String(videoNames[6]);
                break;
            case "refrigerated meat":
                answer=new String(videoNames[1]);
                break;
            case "sauseges":
                answer=new String(videoNames[7]);
                break;
            case "soft cheese":
                answer=new String(videoNames[1]);
                break;
            case "eggs":
                answer=new String(videoNames[1]);
                break;
            case "alcohol":
                answer=new String(videoNames[0]);
                break;
            case "juice":
                answer=new String(videoNames[8]);
                break;
            case "ice cream":answer=new String(videoNames[9]);
                break;
        }
        }
        return answer;
    }
    private String randomizedAnswerSelect(String [] answers){
        Random random=new Random();
        return answers[random.nextInt(answers.length)];
    }
}