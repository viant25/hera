package com.example.user.hera;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

public class SingUp extends AppCompatActivity implements View.OnClickListener{

    Button mRegister;
    EditText mEmail,mPassword;
    TextView mBlogin,mBforgotPass;

    private FirebaseAuth mAuth;
    // create refernce to storage
    private StorageReference mStorage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);

        //set
        mRegister = (Button) findViewById(R.id.restBtn);
        mEmail = (EditText) findViewById(R.id.email_input);
        mPassword = (EditText) findViewById(R.id.pass_input);
        mBlogin = (TextView) findViewById(R.id.logingMe);
        mBforgotPass = (TextView) findViewById(R.id.forgot_password);
        //set onClick
        mBlogin.setOnClickListener(this);
        mBforgotPass.setOnClickListener(this);
        mRegister.setOnClickListener(this);

        //set firebase Auth..with it u have connect to the data in firebase Auth
        mAuth = FirebaseAuth.getInstance();

        //get refernce to storage
        mStorage = FirebaseStorage.getInstance().getReference();

    }

    @Override
    public void onClick(View view) {

        // if u click login button u go to activity MainActivity
        if(view.getId() == R.id.logingMe){
            startActivity(new Intent(SingUp.this,MainActivity.class));
            finish();
        }
        // if u click forgot button u go to activity ForgotPassword
        else if(view.getId() == R.id.forgot_password){
            startActivity(new Intent(SingUp.this,ForgotPassword.class));
            finish();
        }
        // if u click register button u go to singUpUser function
        else if(view.getId() == R.id.restBtn){

            singUpUser(mEmail.getText().toString(),mPassword.getText().toString());
            startActivity(new Intent(SingUp.this,DashBord.class));

        }

    }
    // singUpUser make the user open account and make his email and password save in firebase
    private void singUpUser(String email, String password) {

        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful()){
                    Toast.makeText(SingUp.this,"Error: "+task.getException(),Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(SingUp.this,"Register success",Toast.LENGTH_LONG).show();
                }
            }
        });

        //String userID_By_Email = email + "1";
        //creating new storage for the new user - for each user there will be place
        // for photos,movies and documents to store via the app
        //whenever in the future we will want to put files in those storage places,
        //we will need to use another .child().putfile("/URI HERE/") and it will be created
        //under the photos/docs/vids accordingly
        File fi = null;
        try{
            fi = File.createTempFile("test2","txt");
        } catch(IOException e){}
        //photos
        StorageReference filepathPhotos = mStorage.child(email).child("Photos");
        StorageReference filepathPhotosUltra = mStorage.child(email).child("Photos").child("ultrasound");
        StorageReference filepathPhotosChildhood = mStorage.child(email).child("Photos").child("childhood");
        StorageReference filepathPhotosSelfies = mStorage.child(email).child("Photos").child("selfies");
        StorageReference filepathPhotosBellyProgress = mStorage.child(email).child("Photos").child("bellyProgress");
        StorageReference filepathPhotosProducts = mStorage.child(email).child("Photos").child("products");
        StorageReference filepathPhotosFamily = mStorage.child(email).child("Photos").child("family");
        //Docs
        StorageReference filepathDocs = mStorage.child(email).child("Documents");
        StorageReference filepathDocsHealth = mStorage.child(email).child("Documents").child("health");
        StorageReference filepathDocsWishList = mStorage.child(email).child("Documents").child("wishList");
        //videos
        StorageReference filepathVids = mStorage.child(email).child("Videos");
        //gps
        StorageReference filepathGPS = mStorage.child(email).child("GPS");
        StorageReference filepathGPSHospital = mStorage.child(email).child("GPS").child("hospital");
        StorageReference filepathGPSClinic = mStorage.child(email).child("GPS").child("clinic");
        StorageReference filepathGPSRecommended = mStorage.child(email).child("GPS").child("recommended");


        //filepathDocs.putFile(Uri.fromFile(fi));
        Uri pictureFile = Uri.fromFile(new File("@drawable/female"));
        filepathDocs.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathDocsHealth.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathDocsWishList.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathGPS.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathGPSClinic.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathGPSHospital.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathGPSRecommended.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathPhotos.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathPhotosBellyProgress.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathPhotosChildhood.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathPhotosFamily.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathPhotosProducts.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathPhotosSelfies.child("testdoc.txt").putFile(pictureFile);
        filepathPhotosUltra.child("testdoc.txt").putFile(Uri.fromFile(fi));
        filepathVids.child("testdoc.txt").putFile(Uri.fromFile(fi));


    }
}