package com.example.user.hera;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DocumentListActivity extends AppCompatActivity {

    private DatabaseReference mDatabaseRef;
    private List<DocumentUpload> imgList;
    private ListView lv;
    private DocumentListAdapter adapter;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_document_list);
        imgList = new ArrayList<>();
        lv = (ListView)findViewById(R.id.listViewImage2);
        //Show progress dialog during list image loading
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait loading list image...");
        progressDialog.show();

        //mDatabaseRef = FirebaseDatabase.getInstance().getReference(Download.FB_DATABASE_PATH);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(download_documents.FB_DATABASE_PATH);

        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressDialog.dismiss();

                //Fetch image data from firebase database
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    //ImageUpload class require default constructor
                    //ImageUpload img = snapshot.getValue(ImageUpload.class);
                    DocumentUpload doc = snapshot.getValue(DocumentUpload.class);
                    //imgList.add(img);
                    imgList.add(doc);
                }


                //Init adapter
                adapter = new DocumentListAdapter(DocumentListActivity.this, R.layout.document_item, imgList);
                //Set adapter for listview
                lv.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                progressDialog.dismiss();
            }
        });

    }

}

