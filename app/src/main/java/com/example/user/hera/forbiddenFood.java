package com.example.user.hera;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class forbiddenFood extends AppCompatActivity implements View.OnClickListener  {


    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forbidden_food);

        toolbar = (Toolbar) findViewById(R.id.include);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Forbidden Food");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.forbiddenFood_drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();



        NavigationView mNavigationView = (NavigationView) findViewById(R.id.forbiddenFood_navView);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case(R.id.my_hera): {
                        startActivity(new Intent(forbiddenFood.this, StartActivity.class));
                        break;
                    }
                    case(R.id.weekly_tests): {
                        startActivity(new Intent(forbiddenFood.this, WeeklyTests.class));
                        break;
                    }
                    case(R.id.download_images): {
                        startActivity(new Intent(forbiddenFood.this, Download.class));
                        break;
                    }
                    case(R.id.forbidden_food): {
                        startActivity(new Intent(forbiddenFood.this, forbiddenFood.class));
                        break;
                    }
                    case(R.id.menu_reminder): {
                        startActivity(new Intent(forbiddenFood.this, ReminderActivity.class));
                        break;
                    }
                    case (R.id.upload_pics):
                        startActivity(new Intent(forbiddenFood.this, upload_pictures_types.class));
                        break;
                }
                return true;

            }
        });



        ImageView meatImg = (ImageView) findViewById(R.id.meat);
      //  TextView meatTxt = (TextView) findViewById((R.id.meat_txt));
        meatImg.setOnClickListener(this);
        //meatTxt.setOnClickListener(this);
        ImageView fishImg = (ImageView) findViewById(R.id.fish);
        //TextView fishTxt = (TextView) findViewById((R.id.fill_text));
        fishImg.setOnClickListener(this);
        //fishTxt.setOnClickListener(this);
        ImageView eggsImg = (ImageView) findViewById(R.id.eggs);
        //TextView eggsTxt = (TextView) findViewById((R.id.eggs_txt));
        eggsImg.setOnClickListener(this);
        //eggsTxt.setOnClickListener(this);
        ImageView cheeseImg = (ImageView) findViewById(R.id.cheese);
        //TextView cheeseTxt = (TextView) findViewById((R.id.cheese_txt));
        cheeseImg.setOnClickListener(this);
        //cheeseTxt.setOnClickListener(this);
        ImageView iceCreamImg = (ImageView) findViewById(R.id.ice_cream);
        //TextView iceCreamTxt = (TextView) findViewById((R.id.ice_cream_txt));
        iceCreamImg.setOnClickListener(this);
        //iceCreamTxt.setOnClickListener(this);
        ImageView drinksImg = (ImageView) findViewById(R.id.drinks);
        //TextView drinksTxt = (TextView) findViewById((R.id.drinks_txt));
        drinksImg.setOnClickListener(this);
        //drinksTxt.setOnClickListener(this);
        }

    @Override
    public void onClick(View v) {
        TextView header=(TextView)findViewById(R.id.food_header);
        TextView forbidden=(TextView)findViewById(R.id.forbidden);
        TextView tips=(TextView)findViewById(R.id.food_tips);
        TextView tipsHeader=(TextView)findViewById(R.id.tips_header);
        TextView forbHeader=(TextView)findViewById(R.id.forb_header);
        String tipsStr;String forbStr;String headStr;



        switch (v.getId()){
            case R.id.meat:
                 headStr="Meat and Poultry";
                 forbStr="Raw or undercooked meat or poultry\n"+
                                "Refrigerated meat of any kind (including ham, turkey, roast beef, hot dogs, bologna, prosciutto" +
                                ", and pâté) unless heated until steaming (165° F)\n"+"" +
                                "Dry, uncooked sausages (such as salami and pepperoni) unless heated until steaming";
                 tipsStr="Use a food thermometer. Cook beef, veal, and lamb to 65° F. Cook pork and all ground meats to 72° C. Cook poultry to 75° C.";

                header.setText(headStr);
                forbidden.setText(forbStr);
                forbidden.setVisibility(View.VISIBLE);
                tips.setText(tipsStr);
                tips.setVisibility(View.VISIBLE);
                forbHeader.setVisibility(View.VISIBLE);
                tipsHeader.setVisibility(View.VISIBLE);

                break;
        //    case R.id.meat_txt:
            //    break;
            case R.id.fish:
                headStr="Fish";
                 forbStr="Raw or undercooked fish or shellfish (such as oysters and clams)\n" +
                        "Fish with high levels of mercury, including shark, swordfish, king mackerel, and tilefish (golden or white snapper)\n" +
                        "Refrigerated smoked or pickled fish that's unpasteurized, unless heated until steaming (165° F)\n" +
                        "More than 6 ounces (one serving) a week of canned tuna";
                tipsStr="Cook fish to 145° Fahrenheit or until opaque in the center.\n" +
                        "Eat up to 12 ounces (two servings) a week of low-mercury fish, such as salmon, shrimp, pollock, tilapia, or trout.";
                header.setText(headStr);
                forbidden.setText(forbStr);
                forbidden.setVisibility(View.VISIBLE);
                tips.setText(tipsStr);
                tips.setVisibility(View.VISIBLE);
                forbHeader.setVisibility(View.VISIBLE);
                tipsHeader.setVisibility(View.VISIBLE);
                break;
          //  case R.id.fish_text:
             //   break;
            case R.id.eggs:
                headStr="Eggs";
                forbStr="Runny or undercooked eggs\n" +
                        "Raw cookie dough or cake batter that contains raw eggs\n" +
                        "Homemade desserts or sauces that contain raw eggs (such as eggnog, " +
                        "ice cream, custard, chocolate mousse, hollandaise sauce, béarnaise sauce, mayonnaise, and Caesar salad dressing)";
                tipsStr="Cook eggs until yolks are firm. Cook other dishes containing eggs to 160° F.\n" +
                        "Use pasteurized eggs or a pasteurized egg product when making food that calls for uncooked eggs.";
                header.setText(headStr);
                forbidden.setText(forbStr);
                forbidden.setVisibility(View.VISIBLE);
                tips.setText(tipsStr);
                tips.setVisibility(View.VISIBLE);
                forbHeader.setVisibility(View.VISIBLE);
                tipsHeader.setVisibility(View.VISIBLE);
                break;
           // case R.id.eggs_txt:
             //   break;
            case R.id.cheese:
                headStr="Cheese";
                forbStr="Unpasteurized soft cheese. Nearly all cheeses are pasteurized. Just make sure the label says so, particularly on soft cheese. Don't eat any uncooked food made from raw, unpasteurized milk.";
                header.setText(headStr);
                forbidden.setText(forbStr);
                forbidden.setVisibility(View.VISIBLE);
                forbHeader.setVisibility(View.VISIBLE);
             //   tipsHeader.setVisibility(View.VISIBLE);
                break;
            //case R.id.cheese_txt:
              //  break;
            case R.id.ice_cream:
                headStr="Ice Cream";
                forbStr="Homemade Ice cream may contain raw eggs, which is not suitable or safe when you're pregnant, due to risk of salmonella.\n" +
                        "You should also check that the Ice cream is made from pasteurized milk.";
                tipsStr="";
                header.setText(headStr);
                forbidden.setText(forbStr);
                forbidden.setVisibility(View.VISIBLE);
                forbHeader.setVisibility(View.VISIBLE);
                //tipsHeader.setVisibility(View.VISIBLE);

                break;
            //case R.id.ice_cream_txt:
              //  break;
            case R.id.drinks:
                headStr="Beverages";
                forbStr="Alcoholic beverages.\n" +
                        "Unpasteurized (raw) milk.\n" +
                        "Unpasteurized or fresh squeezed juice from a juice bar or grocery store.\n" +
                        "More than 200 mg of caffeine per day.";
                tipsStr="Be aware of how much caffeine is in tea, soft drinks, energy drinks, chocolate, and coffee ice cream.\n" +
                        "Wash fruit thoroughly before squeezing it for fresh juice." +
                        " (Making your own fresh juice at home is safer than buying fresh squeezed juice from a juice bar or grocery store because you can't be sure how safely fruit has been handled in a retail setting.)";
                header.setText(headStr);
                forbidden.setText(forbStr);
                forbidden.setVisibility(View.VISIBLE);
                tips.setText(tipsStr);
                tips.setVisibility(View.VISIBLE);
                forbHeader.setVisibility(View.VISIBLE);
                tipsHeader.setVisibility(View.VISIBLE);

                break;
            //case R.id.drinks_txt:
              //  break;
    }
}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
