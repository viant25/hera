package com.example.user.hera;

/**
 * Created by dana on 24-Jul-17.
 */

public class DocumentUpload {

    public String name;
    public String url;

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public DocumentUpload(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public DocumentUpload(){}
}
