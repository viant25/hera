package com.example.user.hera;

/**
 * Created by dana on 08-Aug-17.
 */

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class download_documents extends AppCompatActivity {
    private Button mDownloadDocument;
    private StorageReference mstorage;
    static FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    public static final String FB_STORAGE_PATH = "" + user.getUid() + "/Documents/";
    public static final String FB_VIDEOS_STORAGE_PATH = "" + user.getUid() + "/Videos/";
    public static final String FB_DATABASE_PATH = "Users/" + user.getUid() + "/Documents/";
    public static final String FB_VIDEOS_DATABASE_PATH = "Users/" + user.getUid() + "/Videos/";
    private DatabaseReference mDatabaseRef;
    private EditText txtImageName;
    private ProgressDialog mprogress;

    private Uri imgUri;
    public static String fbdatabasepathDocsDir = "Users/" + user.getUid() + "/Documents" + "/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donwnload_documents);
        mDownloadDocument = (Button)findViewById(R.id.btnDownloadVideo);
        mstorage = FirebaseStorage.getInstance().getReference();
        mprogress = new ProgressDialog(this);
        txtImageName = (EditText) findViewById(R.id.textEditDocName);
    }

    public void btnBrowse_Click(View v) {
        Intent intent = new Intent();
        //intent.setType("application/pdf | application/msword | application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        intent.setType("text/plain");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivity(intent);
        startActivityForResult(Intent.createChooser(intent, "Select image"), 1);
    }

    public void btnBrowseVideos_Click(View v) {
        Intent intent = new Intent();
        //intent.setType("application/pdf | application/msword | application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivity(intent);
        startActivityForResult(Intent.createChooser(intent, "Select video"), 2);
    }

    public String getImageExt(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @SuppressWarnings("VisibleForTests")
    public void btnDocsUpload(View v) {
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your document");
            dialog.show();

            //Get the storage reference
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + System.currentTimeMillis() + "." + getImageExt(imgUri));
            //mDatabaseRef = FirebaseDatabase.getInstance().getReference(FB_DATABASE_PATH + "Documents/");
            //StorageReference ref = mstorage.child("images/" + System.currentTimeMillis() + "." + getImageExt(imgUri));

            //Add file to reference

            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    //Dimiss dialog when success
                    dialog.dismiss();
                    //Display success toast msg
                    Toast.makeText(getApplicationContext(), "Document uploaded", Toast.LENGTH_SHORT).show();
                    DocumentUpload documentUpload = new DocumentUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());
                    mDatabaseRef = FirebaseDatabase.getInstance().getReference(FB_DATABASE_PATH + "/");

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.push().getKey();
                    mDatabaseRef.child(uploadId).setValue(documentUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            //Dimiss dialog when error
                            dialog.dismiss();
                            //Display err toast msg
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            //Show upload progress

                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select document", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("VisibleForTests")
    public void btnVideosUpload(View v) {
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your document");
            dialog.show();

            //Get the storage reference
            StorageReference ref = mstorage.child(FB_VIDEOS_STORAGE_PATH + System.currentTimeMillis() + "." + getImageExt(imgUri));
            //mDatabaseRef = FirebaseDatabase.getInstance().getReference(FB_DATABASE_PATH + "Documents/");
            //StorageReference ref = mstorage.child("images/" + System.currentTimeMillis() + "." + getImageExt(imgUri));

            //Add file to reference

            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    //Dimiss dialog when success
                    dialog.dismiss();
                    //Display success toast msg
                    Toast.makeText(getApplicationContext(), "Video uploaded", Toast.LENGTH_SHORT).show();
                    DocumentUpload documentUpload = new DocumentUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());
                    mDatabaseRef = FirebaseDatabase.getInstance().getReference(FB_VIDEOS_DATABASE_PATH + "/");
//j
                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.push().getKey();
                    mDatabaseRef.child(uploadId).setValue(documentUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            //Dimiss dialog when error
                            dialog.dismiss();
                            //Display err toast msg
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            //Show upload progress

                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select document", Toast.LENGTH_SHORT).show();
        }
    }

    public void btnShowListDocument_Click(View v) {
        //fbdatabasepathDocsDir = fbdatabasepathDocsDir;
        Intent i = new Intent(download_documents.this, DocumentListActivity.class);
        startActivity(i);
    }

    public void btnShowListVideos_Click(View v) {
        fbdatabasepathDocsDir = fbdatabasepathDocsDir;
        // TODO:                                        CHANGE THIS!!!
        Intent i = new Intent(download_documents.this, DocumentListActivity.class);
        startActivity(i);
    }

    @SuppressWarnings("VisibleForTests")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {

            Uri uri = data.getData();
            imgUri = data.getData();

            /*
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + System.currentTimeMillis() + "." + getImageExt(imgUri));

            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    //Dimiss dialog when success
                    mprogress.dismiss();
                    //Display success toast msg
                    Toast.makeText(getApplicationContext(), "Document uploaded", Toast.LENGTH_SHORT).show();
                    DocumentUpload documentUpload = new DocumentUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.push().getKey();
                    mDatabaseRef.child(uploadId).setValue(documentUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            //Dimiss dialog when error
                            mprogress.dismiss();
                            //Display err toast msg
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            //Show upload progress

                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            mprogress.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });

                    */


//            if (requestCode == 2 && resultCode == RESULT_OK) {
//                mprogress.setMessage("Uploading...");
//                mprogress.show();
//                Uri uri_vid = data.getData();
//                imgUri = data.getData();
//                StorageReference ref_vid = mstorage.child(FB_STORAGE_PATH + "/videos" + System.currentTimeMillis() + "." + getImageExt(imgUri));
//
//                ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                    @Override
//                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
//
//                        //Dimiss dialog when success
//                        mprogress.dismiss();
//                        //Display success toast msg
//                        Toast.makeText(getApplicationContext(), "Video uploaded", Toast.LENGTH_SHORT).show();
//                        DocumentUpload documentUpload = new DocumentUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());
//
//                        //Save image info in to firebase database
//                        String uploadId = mDatabaseRef.push().getKey();
//                        mDatabaseRef.child(uploadId).setValue(documentUpload);
//
//                    }
//                })
//                        .addOnFailureListener(new OnFailureListener() {
//                            @Override
//                            public void onFailure(@NonNull Exception e) {
//
//                                //Dimiss dialog when error
//                                mprogress.dismiss();
//                                //Display err toast msg
//                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        })
//                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
//
//                            @Override
//                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
//
//                                //Show upload progress
//
//                                double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
//                                mprogress.setMessage("Uploaded " + (int) progress + "%");
//                            }
//                        });
//            }
        }

        if (requestCode == 2 && resultCode == RESULT_OK) {

            Uri uri = data.getData();
            imgUri = data.getData();
        }
    }

/*
    public void btnUpload(View v) {
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your image");
            dialog.show();

            //Get the storage reference
            //TODO:                                         change the path here
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + "health/" + System.currentTimeMillis() + "." + getImageExt(imgUri));
            //StorageReference ref = mstorage.child("images/" + System.currentTimeMillis() + "." + getImageExt(imgUri));

            //Add file to reference

            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    //Dimiss dialog when success
                    dialog.dismiss();
                    //Display success toast msg
                    Toast.makeText(getApplicationContext(), "Image uploaded", Toast.LENGTH_SHORT).show();
                    ImageUpload imageUpload = new ImageUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.push().getKey();
                    mDatabaseRef.child(uploadId).setValue(imageUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            //Dimiss dialog when error
                            dialog.dismiss();
                            //Display err toast msg
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            //Show upload progress

                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select image", Toast.LENGTH_SHORT).show();
        }
    }
    */

    // 12:32 9.8.17
    private void downloadFile() {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference storageRef = storage.getReferenceFromUrl("<your_bucket>");
        StorageReference  islandRef = storageRef.child("file.txt");
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(download_documents.FB_STORAGE_PATH);
        File rootPath = new File(Environment.getExternalStorageDirectory(), "file_name");
        if(!rootPath.exists()) {
            rootPath.mkdirs();
        }

        final File localFile = new File(rootPath,"imageName.txt");
        //TODO:                             change the path here
        storageRef.child(mDatabaseRef.toString() + "file.txt").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'


                storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Log.e("firebase ",";local tem file created  created " +localFile.toString());
                        //  updateDb(timestamp,localFile.toString(),position);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e("firebase ",";local tem file not created  created " +exception.toString());
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });


    }

}