package com.example.user.hera;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.MyViewHolder>  {

    private List<Reminders> remindersList ;
    private Context context;

    private SharedPreference sharedPreference = new SharedPreference();

    public ReminderAdapter(List<Reminders> remindersList, Context context) {
        this.remindersList = remindersList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reminder_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReminderAdapter.MyViewHolder holder, final int position) {
        final Reminders reminders = remindersList.get(position);
        holder.title.setText(reminders.getTitle());
        holder.date.setText(reminders.getDate());
        holder.time.setText(reminders.getTime());


        holder.date.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                remindersList.remove(position);
                sharedPreference.storeFavorites(context,remindersList);
                notifyItemRemoved(position);

                return false;
            }
        });

        holder.time.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                remindersList.remove(position);
                sharedPreference.storeFavorites(context,remindersList);
                notifyItemRemoved(position);

                return false;
            }
        });

        holder.title.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                remindersList.remove(position);
                sharedPreference.storeFavorites(context,remindersList);
                notifyItemRemoved(position);

                return false;
            }
        });

    }



    @Override
    public int getItemCount() {
        return remindersList.size();
    }




    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title,date,time;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.list_TITLE);
            time = (TextView) itemView.findViewById(R.id.list_time);
            date = (TextView) itemView.findViewById(R.id.list_date);
        }
    }

}
