package com.example.user.hera;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.Weeks;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class WeeklyTests extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private Toolbar toolbar;
    private int weeksPregnant;
    public WeeklyTests(){

    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weeklytests);
        SharedPreferences sharedPref= getSharedPreferences("signUpInfo",MODE_PRIVATE);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String pregWeek=sharedPref.getString("pregWeek","");
        String signDate=sharedPref.getString("signDate","");
        int pregWeekDate=Integer.parseInt(pregWeek);
        Date signDateDate=new Date(signDate);
        Date currentDate=new Date();
        DateTime d1=new DateTime(signDateDate);
        DateTime d2=new DateTime(currentDate);
        weeksPregnant= Weeks.weeksBetween(d1,d2).getWeeks()+pregWeekDate;
        toolbar = (Toolbar) findViewById(R.id.include);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Weekly Tests");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.Week_drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.Week_navView);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case(R.id.my_hera): {
                        startActivity(new Intent(WeeklyTests.this, StartActivity.class));
                        break;
                    }
                    case(R.id.weekly_tests): {
                        startActivity(new Intent(WeeklyTests.this, WeeklyTests.class));
                        break;
                    }
                    case(R.id.download_images): {
                        startActivity(new Intent(WeeklyTests.this, Download.class));
                        break;
                    }
                    case(R.id.forbidden_food): {
                        startActivity(new Intent(WeeklyTests.this, forbiddenFood.class));
                        break;
                    }
                    case(R.id.menu_reminder): {
                        startActivity(new Intent(WeeklyTests.this, ReminderActivity.class));
                        break;
                    }
                    case (R.id.upload_pics):
                        startActivity(new Intent(WeeklyTests.this, upload_pictures_types.class));
                        break;
                }
                return true;

            }
        });

        TextView thisWeekTests = (TextView) findViewById(R.id.ThisWeekTest);
        ImageView weeksImage=(ImageView) findViewById(R.id.weeksImg);

        switch (weeksPregnant){
            case 1:{weeksImage.setImageResource(R.drawable.w01);break;}
            case 2:{weeksImage.setImageResource(R.drawable.w02);break;}
            case 3:{weeksImage.setImageResource(R.drawable.w03);break;}
            case 4:{weeksImage.setImageResource(R.drawable.w04);break;}
            case 5:{weeksImage.setImageResource(R.drawable.w05);break;}
            case 6:{weeksImage.setImageResource(R.drawable.w06);break;}
            case 7:{weeksImage.setImageResource(R.drawable.w07);break;}
            case 8:{weeksImage.setImageResource(R.drawable.w08);break;}
            case 9:{weeksImage.setImageResource(R.drawable.w09);break;}
            case 10:{weeksImage.setImageResource(R.drawable.w10);break;}
            case 11:{weeksImage.setImageResource(R.drawable.w11);break;}
            case 12:{weeksImage.setImageResource(R.drawable.w12);break;}
            case 13:{weeksImage.setImageResource(R.drawable.w13);break;}
            case 14:{weeksImage.setImageResource(R.drawable.w14);break;}
            case 15:{weeksImage.setImageResource(R.drawable.w15);break;}
            case 16:{weeksImage.setImageResource(R.drawable.w16);break;}
            case 17:{weeksImage.setImageResource(R.drawable.w17);break;}
            case 18:{weeksImage.setImageResource(R.drawable.w18);break;}
            case 19:{weeksImage.setImageResource(R.drawable.w19);break;}
            case 20:{weeksImage.setImageResource(R.drawable.w20);break;}
            case 21:{weeksImage.setImageResource(R.drawable.w21);break;}
            case 22:{weeksImage.setImageResource(R.drawable.w22);break;}
            case 23:{weeksImage.setImageResource(R.drawable.w23);break;}
            case 24:{weeksImage.setImageResource(R.drawable.w24);break;}
            case 25:{weeksImage.setImageResource(R.drawable.w25);break;}
            case 26:{weeksImage.setImageResource(R.drawable.w26);break;}
            case 27:{weeksImage.setImageResource(R.drawable.w27);break;}
            case 28:{weeksImage.setImageResource(R.drawable.w28);break;}
            case 29:{weeksImage.setImageResource(R.drawable.w29);break;}
            case 30:{weeksImage.setImageResource(R.drawable.w30);break;}
            case 31:{weeksImage.setImageResource(R.drawable.w31);break;}
            case 32:{weeksImage.setImageResource(R.drawable.w32);break;}
            case 33:{weeksImage.setImageResource(R.drawable.w33);break;}
            case 34:{weeksImage.setImageResource(R.drawable.w34);break;}
            case 35:{weeksImage.setImageResource(R.drawable.w35);break;}
            case 36:{weeksImage.setImageResource(R.drawable.w36);break;}
            case 37:{weeksImage.setImageResource(R.drawable.w37);break;}
            case 38:{weeksImage.setImageResource(R.drawable.w38);break;}
            case 39:{weeksImage.setImageResource(R.drawable.w39);break;}
            case 40:{weeksImage.setImageResource(R.drawable.w40);break;}
            case 41:{weeksImage.setImageResource(R.drawable.w41);break;}
            case 42:{weeksImage.setImageResource(R.drawable.w42);break;}
        }

        if (weeksPregnant >= 1 && weeksPregnant <= 8) {
            String s = "Between weeks 7 to 8 you should take an ultrasound to to determine the location of the pregnancy sac, size and vitality (heartbeat) of the fetus.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant == 10) {
            thisWeekTests.setSingleLine(false);
            String s = "Strating this week you will receive a tracking card for your routine blood tests exams.\n"+
                        "There are sevral routine tests you should take: Blood count, fasting glucose, " +
                        "urine culture, blood type (and RH antibodies level if negative), " +
                        "rubella antibodies, VDRL, CMV, antibodies, toxoplasma and HBsAG.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant >= 11 && weeksPregnant <= 12) {
            String s = "In weeks 11 to 12, you should take an early chromosomal genetic diagnostic testing, placental structure (chorionic placenta).\n"+"" +
                    "You should also schedule an ultrasound for nuchal translucency or fetal development test performed by the physician or technician.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant >= 15 && weeksPregnant <= 16) {
            String s = "This week you need an early ultrasound screening exam.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant == 17) {
            String s = "This week you need to take a triple screen test, fetoprotein or amniocentesis and blood count.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant >= 22 && weeksPregnant <= 25) {
            String s = "During weeks 22 to 25 you should take late ultrasound anomaly scan.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant == 26) {
            String s = "At week 26, you need to take glucose tolerance test, 50-100 grams (based on risk factors), blood count and RH antibody levels for those with negative blood type and urine culture.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant >= 28) {
            String s = "Schedule a visit to the physician to discuss glucose test results and talk about receiving RH immune globulin if RH negative\n.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant >= 29 && weeksPregnant <= 30) {
            String s = "You need a doctor's review of fetal growth.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant == 32) {
            String s = "You need a follow up review";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant >= 34 && weeksPregnant <= 39) {
            String s = "Doctor's review";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant == 40) {
            String s = "From this week you need to take an ultrasound to monitor biophysical profile every 2-4 days.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        } else if (weeksPregnant >= 41 && weeksPregnant <= 42) {
            String s = "Referral to the delivery room to perform a stress test or induce labor.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        }
        else{
            String s = "Nothing special needed.";
            thisWeekTests.setText(s);
            thisWeekTests.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
