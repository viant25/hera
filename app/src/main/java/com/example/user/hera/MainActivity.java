package com.example.user.hera;
//////
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // Google sing in
    private SignInButton mGoogleBtn;
    private static final int RC_SIGN_IN = 1;
    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "MAIN_ACTIVITY";
    // End - Google sing in

    Button mLogin;
    EditText mEmail,mPassword;
    TextView mBsingup,mBforgotPass;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    public static final String FB_STORAGE_PATH = "Photos/";
    public static final String FB_DATABASE_PATH = "Photos";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ===== Google sing in =======
        mGoogleBtn = (SignInButton) findViewById(R.id.googleBtn);

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(MainActivity.this,"you got an Error",Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();

        mGoogleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                signIn();
                if (mAuth.getCurrentUser() == null) {
                    // User is signed in
                    startActivity(new Intent(MainActivity.this, DashBord.class));
                }

            }
        });

    // ===== End - Google sing in ===========

/*        Firebase ref = new Firebase("https://hera-2c86a.firebaseio.com/");
        ref.addAuthStateListener(new Firebase.AuthStateListener() {
            @Override
            public void onAuthStateChanged(AuthData authData) {
                if (authData != null) {
                    // user is logged in
                } else {
                    // user is not logged in
                }
            }
        });*/

        //set
        mLogin = (Button) findViewById(R.id.restBtn);
        mEmail = (EditText) findViewById(R.id.email_input);
        mPassword = (EditText) findViewById(R.id.pass_input);
        mBsingup = (TextView) findViewById(R.id.forgot_singup);
        mBforgotPass = (TextView) findViewById(R.id.forgot_password);

        //set onClick
        mBsingup.setOnClickListener(this);
        mBforgotPass.setOnClickListener(this);
        mLogin.setOnClickListener(this);


        mAuth = FirebaseAuth.getInstance();
       /* mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //FirebaseUser user = firebaseAuth.getCurrentUser();
                if (mAuth.getCurrentUser() == null) {
                    // User is signed in
                    startActivity(new Intent(MainActivity.this,DashBord.class));
                    //Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    startActivity(new Intent(MainActivity.this,StartActivity.class));
                    // Log.d(TAG, "onAuthStateChanged:signed_out");
                }*/
                // ...
            //}
       // };

        // check already session, if ok -> DashBord
        if(mAuth.getCurrentUser() != null) {
            startActivity(new Intent(MainActivity.this, StartActivity.class));
        }

    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//
//        mAuth.addAuthStateListener(mAuthListener);
//    }

    // ======= Google sing in =========
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });

    }

    //======= End - Google sing in  ================================================


    @Override
    public void onClick(View view){

        // if u click forgot button u go to activity ForgotPassword
        if(view.getId() == R.id.forgot_password){
            //startActivity(new Intent(MainActivity.this,ForgotPassword.class));
            startActivity(new Intent(MainActivity.this,Download.class));
            finish();
        }
        // if u click singup button u go to activity SingUp
        else if(view.getId() == R.id.forgot_singup){
            startActivity(new Intent(MainActivity.this,SingUp.class));
            finish();
        }
        // if u click register button u go to to function loginUser
        else if(view.getId() == R.id.restBtn){

            loginUser(mEmail.getText().toString().trim(),mPassword.getText().toString().trim());
        }

    }

    // with loginUser the user put is email and password and than go to DashBord activity and is check if the user have account in firebase
    private void loginUser(String email, final String password) {

        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful()){
                    if(password.length() < 6){
                        Toast.makeText(MainActivity.this,"Password length must be over 6",Toast.LENGTH_LONG).show();
                    }
                }else{
                    //startActivity(new Intent(MainActivity.this,StartActivity.class));
                    startActivity(new Intent(MainActivity.this,StartActivity.class));
                }
            }
        });
    }
}