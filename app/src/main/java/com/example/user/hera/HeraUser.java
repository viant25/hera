package com.example.user.hera;

/**
 * Created by dana on 28-Jul-17.
 */


public class HeraUser {
    //name and address string
    //private String mFillName,mFillAge,weekPreg,mFillCity,mFillBabyGender,mFillBloodType,mStartDate,email;
    private String Age,BabyGender,BloodType,City,Name, PregStartWeek, email, signDate;

    public HeraUser() {
  /*Blank default constructor essential for Firebase*/
    }

    public HeraUser(String age, String babyGender, String bloodType, String city, String name, String pregStartWeek, String email, String signDate) {
        Age = age;
        BabyGender = babyGender;
        BloodType = bloodType;
        City = city;
        Name = name;
        PregStartWeek = pregStartWeek;
        this.email = email;
        this.signDate = signDate;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getBabyGender() {
        return BabyGender;
    }

    public void setBabyGender(String babyGender) {
        BabyGender = babyGender;
    }

    public String getBloodType() {
        return BloodType;
    }

    public void setBloodType(String bloodType) {
        BloodType = bloodType;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPregStartWeek() {
        return PregStartWeek;
    }

    public void setPregStartWeek(String pregStartWeek) {
        PregStartWeek = pregStartWeek;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }
}

