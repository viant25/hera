package com.example.user.hera;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.api.client.util.DateTime;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


import org.joda.time.Weeks;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;




//added this so upload task can be done

public class Agenda extends AppCompatActivity {
    private Button mSelectImage;
    private StorageReference mstorage;
    private DatabaseReference mDatabaseRef;
    private Button mSelfie;
    private ImageView mImageView;
    private EditText txtImageName;
    private ProgressDialog mprogress;
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int GALLERY_INTENT = 2;
    static FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    public static final String FB_STORAGE_PATH = user.getUid() + "/Photos/";
    public static final String FB_DATABASE_PATH = "Users/" + user.getUid() + "/Photos";
    //public static final String FB_DATABASE_PATH = "Users/Photos";
    //public static final String FB_STORAGE_PATH = "/Photos/";
    public TextView mPregnancyMonth;
    private FirebaseAuth mAuth;

    public static final int REQUEST_CODE = 1234;
    private Uri imgUri;
    public String sign_date;
    public int start_week;
    public int weekPreg;
    public HeraUser heraUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_agenda);
        setContentView(R.layout.activity_agenda);
        heraUser = new HeraUser();

        int testing;
        mstorage = FirebaseStorage.getInstance().getReference();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(FB_DATABASE_PATH);
        //get picture from gallery
        mSelectImage = (Button)findViewById(R.id.btnUpload);
        //selfie with camera
        mSelfie = (Button)findViewById(R.id.btnSelfie);
        mImageView = (ImageView)findViewById(R.id.imageView);
        txtImageName = (EditText) findViewById(R.id.editText2);
        mprogress = new ProgressDialog(this);

        //starts a new intent of gallery - lets user choose a photo from his gallery
        mSelectImage.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                uploadPicture();
                //h
            }
        });

        mSelfie.setOnClickListener( new View.OnClickListener(){
            public void onClick(View view){
                uploadSelfie();
            }
        });
    }

    private void showData(DataSnapshot dataSnapshot) {
        HeraUser heraUser = new HeraUser();
        heraUser.setCity( dataSnapshot.child("City").getValue(HeraUser.class).getCity() );
        Toast.makeText(getApplicationContext(), heraUser.getCity(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode,data);

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imgUri = data.getData();

            try {
                Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                mImageView.setImageBitmap(bm);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode == 42 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imgUri = data.getData();

            try {
                Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                mImageView.setImageBitmap(bm);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //uploads the picture the user chose to the storage path
        if(requestCode == GALLERY_INTENT && resultCode==RESULT_OK){
            mprogress.setMessage("Uploading...");
            mprogress.show();
            Uri uri = data.getData();
            imgUri = data.getData();

            try {
                Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                mImageView.setImageBitmap(bm);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //this is the path that the photo will be uploaded to
            StorageReference filepath = mstorage.child("Photos").child(uri.getLastPathSegment());

            //this is the function that uploads the file to that path (putfile)
            filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>(){
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot){
                    Toast.makeText(Agenda.this, "upload done...", Toast.LENGTH_LONG).show();
                    mprogress.dismiss();


                }
            });
        }
        // this function is used to upload the picture
        if(requestCode == CAMERA_REQUEST_CODE && resultCode==RESULT_OK) {
            mprogress.setMessage("Uploading...");
            mprogress.show();
            Uri uri = data.getData();
            //this is the path that the photo will be uploaded to
            StorageReference filepath = mstorage.child("Photos").child(uri.getLastPathSegment());
            //this is the function that uploads the file to that path (putfile)
            filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(Agenda.this, "upload done...", Toast.LENGTH_LONG).show();
                    mprogress.dismiss();
                }
            });
        }
    }

    public void btnBrowse_Click(View v) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select image"), REQUEST_CODE);
    }

    //here will be functions that helps upload or download media from the storage
    private void uploadPicture(){
        //decide if it is a specific picture with radio buttons
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, GALLERY_INTENT);
    }

    private void uploadSelfie(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_REQUEST_CODE);
    }

    public String getImageExt(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @SuppressWarnings("VisibleForTests")
    public void btnUpload(View v) {
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your image");
            dialog.show();

            //Get the storage reference
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + "bellyProgress/" + System.currentTimeMillis() + "." + getImageExt(imgUri));
            //StorageReference ref = mstorage.child("images/" + System.currentTimeMillis() + "." + getImageExt(imgUri));

            //Add file to reference

            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    //Dimiss dialog when success
                    dialog.dismiss();
                    //Display success toast msg
                    Toast.makeText(getApplicationContext(), "Image uploaded", Toast.LENGTH_SHORT).show();
                    ImageUpload imageUpload = new ImageUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.child("bellyProgress").push().getKey();
                    mDatabaseRef.child(uploadId).setValue(imageUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            //Dimiss dialog when error
                            dialog.dismiss();
                            //Display err toast msg
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            //Show upload progress

                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select image", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("VisibleForTests")
    public void btnUploadProducts(View v) {
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your image");
            dialog.show();
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + "products/" + System.currentTimeMillis() + "." + getImageExt(imgUri));
            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Image uploaded", Toast.LENGTH_SHORT).show();
                    ImageUpload imageUpload = new ImageUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.child("products").push().getKey();
                    mDatabaseRef.child(uploadId).setValue(imageUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            dialog.dismiss();
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select image", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("VisibleForTests")
    public void btnUploadUltrasound(View v) {
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your image");
            dialog.show();
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + "ultrasound/" + System.currentTimeMillis() + "." + getImageExt(imgUri));
            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Image uploaded", Toast.LENGTH_SHORT).show();
                    ImageUpload imageUpload = new ImageUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.child("ultrasound").push().getKey();
                    mDatabaseRef.child(uploadId).setValue(imageUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            dialog.dismiss();
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select image", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("VisibleForTests")
    public void btnUploadFamily(View v) {
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your image");
            dialog.show();
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + "family/" + System.currentTimeMillis() + "." + getImageExt(imgUri));
            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Image uploaded", Toast.LENGTH_SHORT).show();
                    ImageUpload imageUpload = new ImageUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.child("family").push().getKey();
                    mDatabaseRef.child(uploadId).setValue(imageUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            dialog.dismiss();
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select image", Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressWarnings("VisibleForTests")
    public void btnDocsUpload(View v) {
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your document");
            dialog.show();

            //Get the storage reference
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + System.currentTimeMillis() + "." + getImageExt(imgUri));
            //StorageReference ref = mstorage.child("images/" + System.currentTimeMillis() + "." + getImageExt(imgUri));

            //Add file to reference

            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    //Dimiss dialog when success
                    dialog.dismiss();
                    //Display success toast msg
                    Toast.makeText(getApplicationContext(), "Document uploaded", Toast.LENGTH_SHORT).show();
                    DocumentUpload documentUpload = new DocumentUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.push().getKey();
                    mDatabaseRef.child(uploadId).setValue(documentUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            //Dimiss dialog when error
                            dialog.dismiss();
                            //Display err toast msg
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            //Show upload progress

                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select document", Toast.LENGTH_SHORT).show();
        }
    }

    public void getCurrentUsrDetails(View v){
        /*FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String name;
        String email;
        if (user != null) {
            // Name, email address, and profile photo Url
            name = user.getDisplayName();
            email = user.getEmail();
           // Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();

            // Check if user's email is verified
            boolean emailVerified = user.isEmailVerified();

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getToken() instead.
            String uid = user.getUid();
            Toast.makeText(getApplicationContext(), uid, Toast.LENGTH_SHORT).show();
        }*/
        //f();
    }



    ValueEventListener postListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            // Get Post object and use the values to update the UI
            HeraUser heraUser = dataSnapshot.getValue(HeraUser.class);
            //Post post = dataSnapshot.getValue(Post.class);
            // [START_EXCLUDE]
            Toast.makeText(getApplicationContext(), heraUser.getAge(), Toast.LENGTH_SHORT).show();
           // mAuthorView.setText(post.author);
           // mTitleView.setText(post.title);
           // mBodyView.setText(post.body);
            // [END_EXCLUDE]
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            // Getting Post failed, log a message
            //Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            // [START_EXCLUDE]
            Toast.makeText(Agenda.this, "Failed to load post.",
                    Toast.LENGTH_SHORT).show();
            // [END_EXCLUDE]
        }
    };


    public void getSignDate(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference refere = FirebaseDatabase.getInstance().getReference();
        refere = refere.child("Users").child(user.getUid());

       // DatabaseReference ref = mDatabaseRef.child("Users").child(user.getUid());
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference startWeeka = refere.child("signDate").getRef();

        //startYear.child().
        //String startMonth = ref.child("PregStartMonth").getKey();
        refere.child("signDate").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // Toast.makeText(getApplicationContext(), snapshot.getValue().toString(), Toast.LENGTH_SHORT).show();
                sign_date = snapshot.getValue().toString();
                Toast.makeText(getApplicationContext(), sign_date, Toast.LENGTH_SHORT).show();

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        //mDatabaseRef.addValueEventListener(refere);
        refere.child("PregStartWeek").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Toast.makeText(getApplicationContext(), snapshot.getValue().toString(), Toast.LENGTH_SHORT).show();
                start_week = Integer.parseInt(snapshot.getValue().toString());
                //v

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void getWeeksPregnant(String signDate, String pregStartWeek) {


        DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        Calendar cal1  = Calendar.getInstance();
        try {
            cal1.setTime(df.parse(signDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String CurrentDate = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
        DateFormat d = new SimpleDateFormat("yyyy/MM/dd");
        Calendar cal2  = Calendar.getInstance();
        try {
            cal2.setTime(d.parse(CurrentDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        org.joda.time.DateTime dateTime1 = new org.joda.time.DateTime(cal1);
        org.joda.time.DateTime dateTime2 = new org.joda.time.DateTime(cal2);

        int weeks = Weeks.weeksBetween(dateTime1, dateTime2).getWeeks();

        int weeksfinal = Integer.parseInt(pregStartWeek);

         weekPreg = weeks + weeksfinal;


    }

    public void showPregnancyMonth(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference ref = mDatabaseRef.child("Users").child(user.getUid());
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        //mDatabaseRef.child()
        String em = ref.child("email").getKey();
        //Value event listener for realtime data update
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    //Getting the data from snapshot
                    HeraUser person = postSnapshot.getValue(HeraUser.class);
                    //String week = person.getWeekPreg();
                    //Date fictionalDate = new Date(2017,)


                    /**
                    String calculateWeek;

                    Calendar c = Calendar.getInstance();
                    //c.setTime(yourdate); // yourdate is an object of type Date

                    //int currMonth = c.get(Calendar.);
                    int CurrWeek = c.get(Calendar.WEEK_OF_MONTH);
                    Calendar date1 = Calendar.getInstance();
                    Calendar date2 = Calendar.getInstance();

                    date1.clear();
                    date1.set(2015, 12, 29); // set date 1 (yyyy,mm,dd)
                    date2.clear();
                    date2.set(2016, 02, 7); //set date 2 (yyyy,mm,dd)

                    long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

                    float dayCount = (float) diff / (24 * 60 * 60 * 1000);

                    int weeeek = (int) (dayCount / 7);

                    String r = "sd";
                    //mPregnancyMonth.setText(r);
                    //mPregnancyMonth.setText(String.valueOf(weeeek));
                    mPregnancyMonth.setText(week);
                     */

                    //Displaying it on textview
                    //textViewPersons.setText(string);
                    //mPregnancyMonth.setText(week);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });


        //mPregnancyMonth.setText(month);
    }


    public void getNoOfWeek()
    {

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(2015, 12, 29); // set date 1 (yyyy,mm,dd)
        date2.clear();
        date2.set(2016, 02, 7); //set date 2 (yyyy,mm,dd)

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        int week = (int) (dayCount / 7);
        //System.out.println(week);

       // HeraUser person = postSnapshot.getValue(HeraUser.class);
        //String week = person.getWeekPreg();

        //Date pregStartDate = new Date()
    }


    private void uploadUltrasoundPicture(){

    }

    private void downloadPicture(){
        //download it to the device or show it to the user?
    }

    private void uploadDocument(){

    }

    private void downloadDocument(){

    }

    private void addCalendarMeeting(){

    }

    private void showMeetings(){

    }

    public void getWeeks(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String uid = user.getUid();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("Users").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HeraUser user = dataSnapshot.getValue(HeraUser.class);
                heraUser = user;
                getWeeksPregnant(heraUser.getSignDate(),heraUser.getPregStartWeek());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
