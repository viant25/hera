package com.example.user.hera;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class DashBord extends AppCompatActivity implements View.OnClickListener {

    private EditText mFillName,mFillAge,mFillCity,mFillBabyGender,mFillBloodType, mStartWeek;
    private Button mFillBtn,mStarbtn;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_bord);

        //set
        mFillName = (EditText) findViewById(R.id.name_fill);
        mFillAge = (EditText) findViewById(R.id.age_fill);
        mFillCity = (EditText) findViewById(R.id.city_fill);
        mFillBabyGender = (EditText) findViewById(R.id.baby_gender_fill);
        mFillBloodType = (EditText)findViewById(R.id.blood_type_fill);
        mStartWeek = (EditText)findViewById(R.id.week_pregnancy_fill);

        //mFillBtn = (Button) findViewById(R.id.Fillbtn);
        mStarbtn = (Button) findViewById(R.id.startBtn);

        //set the connect to database in firebase
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //set OnClick
        //mFillBtn.setOnClickListener(this);
        mStarbtn.setOnClickListener(this);

    }
    @Override
    public void onClick(View view) {
        //if click on Fill button u go to FillDetail
        //dana: i'm currently commenting it so there will be more space for more user information
        //      and this button is useless cause the information on the user must be filled anyway
        /*
        if(view.getId() == R.id.Fillbtn){
            FillDetails();
        }
        //if click on start button u go to start activity
        else if(view.getId() == R.id.startBtn){
        */
        if(view.getId() == R.id.startBtn){
            FillDetails();
            startActivity(new Intent(DashBord.this,StartActivity.class));
            finish();
        }

    }

    //FillDetails let the user save all the details he write and save it in firebase
    private void FillDetails() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String email;
        email = "error";
        if (user != null) {
            // Name, email address, and profile photo Url
            email = user.getEmail();
            Toast.makeText(getApplicationContext(), email, Toast.LENGTH_SHORT).show();

        }


        String name = mFillName.getText().toString();
        String age = mFillAge.getText().toString().trim();
        String PregStartWeek = mStartWeek.getText().toString().trim();
        String city = mFillCity.getText().toString().trim();
        String babygender = mFillBabyGender.getText().toString().trim();
        String bloodType = mFillBloodType.getText().toString().trim();
        String signDate = "";
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date signUpDate = new Date();
        signDate = signUpDate.toString();
        SharedPreferences sharedPref= getSharedPreferences("signUpInfo",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPref.edit();
        editor.putString("pregWeek",PregStartWeek);
        editor.putString("signDate",signDate);
        editor.commit();
        HashMap<String,String> dataMap = new HashMap<String,String>();

        dataMap.put("Age",age);
        dataMap.put("BabyGender",babygender);
        dataMap.put("BloodType",bloodType);
        dataMap.put("City",city);
        dataMap.put("Name",name);
        dataMap.put("PregStartWeek",PregStartWeek);
        dataMap.put("email",email);
        // TOO: change this to date / string
        dataMap.put("signDate",signDate);

        //should change user02 to something more scalable
        // !!!!!!!!!!!!!!!!!!!!!! change it late to usr id  !!!!!!!!!!!!!!
        mDatabase.child("Users").child(user.getUid()).setValue(dataMap);
    }
}
