package com.example.user.hera;

/**
 * Created by dana on 18-Jul-17.
 */
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import android.view.View;
import android.content.Intent;



public class Download extends AppCompatActivity {
    private ImageButton btnS;
    static FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    public static final String FB_STORAGE_PATH = user.getUid() + user.getUid() + "/Photos/";
    public static final String FB_DATABASE_PATH = "Users/" + user.getUid() + "/Photos";
    public static String fbdatabasepathPhotoDir = "Users/" + user.getUid() + "/Photos" + "/";
    public static String fbdatabasepathDocsDir = "Users/" + user.getUid() + "/Documents" + "/";
    public static String a;
    //private File localFile = null;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        toolbar = (Toolbar) findViewById(R.id.include);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Images");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.download_drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.Download_navView);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case(R.id.my_hera): {
                        startActivity(new Intent(Download.this, StartActivity.class));
                        break;
                    }
                    case(R.id.weekly_tests): {
                        startActivity(new Intent(Download.this, WeeklyTests.class));
                        break;
                    }
                    case(R.id.download_images): {
                        startActivity(new Intent(Download.this, Download.class));
                        break;
                    }
                    case(R.id.forbidden_food): {
                        startActivity(new Intent(Download.this, forbiddenFood.class));
                        break;
                    }
                    case(R.id.menu_reminder): {
                        startActivity(new Intent(Download.this, ReminderActivity.class));
                        break;
                    }
                    case (R.id.upload_pics):{
                        startActivity(new Intent(Download.this, upload_pictures_types.class));
                        break;
                    }
                }
                return true;

            }
        });

    }

    public void userCommand(String pictureType){
        fbdatabasepathPhotoDir = fbdatabasepathPhotoDir + pictureType;
        Intent i = new Intent(Download.this,ImageListActivity.class);
        startActivity(i);
    }
    public void btnShowListImage_Click(View v) { //shows belly progress pictures
        //fbdatabasepathPhotoDir += "bellyProgress/";
        a = "bellyProgress/";

        Intent i = new Intent(Download.this, ImageListActivity.class);
        startActivity(i);
    }

    public void btnShowListDocument_Click(View v) {
        fbdatabasepathDocsDir = fbdatabasepathDocsDir + "Health";
        Intent i = new Intent(Download.this, DocumentListActivity.class);
        startActivity(i);
    }

    public void btnShowProductImage_Click(View v) { //shows products pictures
        //fbdatabasepathPhotoDir = fbdatabasepathPhotoDir + "products/";
        a = "products/";
        Intent i = new Intent(Download.this, ImageListActivity.class); //changeme
        startActivity(i);
    }

    public void btnShowUltrasoundImage_Click(View v) { //shows ultrasound pictures
        //fbdatabasepathPhotoDir = fbdatabasepathPhotoDir + "ultrasound/";
        a = "ultrasound/";
        Intent i = new Intent(Download.this, ImageListActivity.class);//changeme
        startActivity(i);
    }

    public void btnShowFamilyImage_Click(View v) { //shows family pictures
        //fbdatabasepathPhotoDir = fbdatabasepathPhotoDir + "family/";
        a = "family/";
        Intent i = new Intent(Download.this, ImageListActivity.class);//changeme
        startActivity(i);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

/**

public class Download {

    FirebaseStorage storage = FirebaseStorage.getInstance();


    public Download(){

    }

    public Download(int something){

        try {
            //final File loca = File.createTempFile("a","jpg");
            final File localFile = File.createTempFile("images", "jpg");
            storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                //@Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    ImageView mmm;
                    mmm.setImageBitmap(bitmap);

                }
            });
        } catch (IOException e ) {}

    }

}

 */
