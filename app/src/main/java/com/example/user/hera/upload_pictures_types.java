package com.example.user.hera;

/**
 * Created by dana on 18-Jul-17.
 */

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.IOException;


public class upload_pictures_types extends AppCompatActivity {
    private ImageButton btnS;
    private StorageReference mstorage;
    private DatabaseReference mDatabaseRef;
    private EditText txtImageName;
    private ProgressDialog mprogress;
    private static final int GALLERY_INTENT = 2;

    static FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    public static final String FB_STORAGE_PATH = user.getUid() + user.getUid() + "/Photos/";
    public static final String FB_DATABASE_PATH = "Users/" + user.getUid() + "/Photos/";
    public static String fbdatabasepathPhotoDir = "Users/" + user.getUid() + "/Photos" + "/";
    public static String fbdatabasepathDocsDir = "Users/" + user.getUid() + "/Documents" + "/";
    public static final int REQUEST_CODE = 1234;
    private ImageView mImageView;

    //private File localFile = null;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private Toolbar toolbar;
    private Uri imgUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_pictures);

        toolbar = (Toolbar) findViewById(R.id.include);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Images");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mstorage = FirebaseStorage.getInstance().getReference();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(FB_DATABASE_PATH);
        txtImageName = (EditText) findViewById(R.id.editText3);
        mImageView = (ImageView)findViewById(R.id.imageView3);
        mprogress = new ProgressDialog(this);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.download_drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.Download_navView);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case(R.id.my_hera): {
                        startActivity(new Intent(upload_pictures_types.this, StartActivity.class));
                        break;
                    }
                    case(R.id.weekly_tests): {
                        startActivity(new Intent(upload_pictures_types.this, WeeklyTests.class));
                        break;
                    }
                    case(R.id.download_images): {
                        startActivity(new Intent(upload_pictures_types.this, Download.class));
                        break;
                    }
                    case(R.id.forbidden_food): {
                        startActivity(new Intent(upload_pictures_types.this, forbiddenFood.class));
                        break;
                    }
                    case(R.id.menu_reminder): {
                        startActivity(new Intent(upload_pictures_types.this, ReminderActivity.class));
                        break;}
                    case (R.id.upload_pics):{
                            startActivity(new Intent(upload_pictures_types.this, upload_pictures_types.class));
                            break;
                    }
                }
                return true;

            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode,data);

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imgUri = data.getData();

            try {
                Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                mImageView.setImageBitmap(bm);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode == 42 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imgUri = data.getData();

            try {
                Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                mImageView.setImageBitmap(bm);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //uploads the picture the user chose to the storage path
        if(requestCode == GALLERY_INTENT && resultCode==RESULT_OK){
            mprogress.setMessage("Uploading...");
            mprogress.show();
            Uri uri = data.getData();
            imgUri = data.getData();

            try {
                Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                mImageView.setImageBitmap(bm);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //this is the path that the photo will be uploaded to
            StorageReference filepath = mstorage.child("Photos").child(uri.getLastPathSegment());

            //this is the function that uploads the file to that path (putfile)
            filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>(){
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot){
                    Toast.makeText(upload_pictures_types.this, "upload done...", Toast.LENGTH_LONG).show();
                    mprogress.dismiss();


                }
            });
        }
    }

    public void btnBrowse_Click(View v) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select image"), REQUEST_CODE);
    }

    @SuppressWarnings("VisibleForTests")
    public void btnUpload(View v) {
        //btnBrowse_Click(v);
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your image");
            dialog.show();

            //Get the storage reference
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + "bellyProgress/" + System.currentTimeMillis() + "." + getImageExt(imgUri));
            mDatabaseRef = FirebaseDatabase.getInstance().getReference(FB_DATABASE_PATH + "bellyProgress/");
            //StorageReference ref = mstorage.child("images/" + System.currentTimeMillis() + "." + getImageExt(imgUri));

            //Add file to reference

            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    //Dimiss dialog when success
                    dialog.dismiss();
                    //Display success toast msg
                    Toast.makeText(getApplicationContext(), "Image uploaded", Toast.LENGTH_SHORT).show();
                    ImageUpload imageUpload = new ImageUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.push().getKey();
                    mDatabaseRef.child(uploadId).setValue(imageUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            //Dimiss dialog when error
                            dialog.dismiss();
                            //Display err toast msg
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            //Show upload progress

                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select image", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("VisibleForTests")
    public void btnUploadProducts(View v) {
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your image");
            dialog.show();
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + "products/" + System.currentTimeMillis() + "." + getImageExt(imgUri));
            mDatabaseRef = FirebaseDatabase.getInstance().getReference(FB_DATABASE_PATH + "products/");
            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Image uploaded", Toast.LENGTH_SHORT).show();
                    ImageUpload imageUpload = new ImageUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.push().getKey();
                    mDatabaseRef.child(uploadId).setValue(imageUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            dialog.dismiss();
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select image", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("VisibleForTests")
    public void btnUploadUltrasound(View v) {
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your image");
            dialog.show();
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + "ultrasound/" + System.currentTimeMillis() + "." + getImageExt(imgUri));
            mDatabaseRef = FirebaseDatabase.getInstance().getReference(FB_DATABASE_PATH + "ultrasound/");
            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Image uploaded", Toast.LENGTH_SHORT).show();
                    ImageUpload imageUpload = new ImageUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.push().getKey();
                    mDatabaseRef.child(uploadId).setValue(imageUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            dialog.dismiss();
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select image", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("VisibleForTests")
    public void btnUploadFamily(View v) {
        if (imgUri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading your image");
            dialog.show();
            StorageReference ref = mstorage.child(FB_STORAGE_PATH + "family/" + System.currentTimeMillis() + "." + getImageExt(imgUri));
            mDatabaseRef = FirebaseDatabase.getInstance().getReference(FB_DATABASE_PATH + "family/");
            ref.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Image uploaded", Toast.LENGTH_SHORT).show();
                    ImageUpload imageUpload = new ImageUpload(txtImageName.getText().toString(), taskSnapshot.getDownloadUrl().toString());

                    //Save image info in to firebase database
                    String uploadId = mDatabaseRef.push().getKey();
                    mDatabaseRef.child(uploadId).setValue(imageUpload);

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            dialog.dismiss();
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Please select image", Toast.LENGTH_SHORT).show();
        }
    }

    public String getImageExt(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }


    public void btnShowListImage_Click(View v) { //shows belly progress pictures
        fbdatabasepathPhotoDir = fbdatabasepathPhotoDir + "bellyProgress";
        Intent i = new Intent(upload_pictures_types.this, ImageListActivity.class);
        startActivity(i);
    }

    public void btnShowListDocument_Click(View v) {
        fbdatabasepathDocsDir = fbdatabasepathDocsDir + "Health";
        Intent i = new Intent(upload_pictures_types.this, DocumentListActivity.class);
        startActivity(i);
    }

    public void btnShowProductImage_Click(View v) { //shows products pictures
        fbdatabasepathPhotoDir = fbdatabasepathPhotoDir + "products";
        Intent i = new Intent(upload_pictures_types.this, ImageListActivity.class); //changeme
        startActivity(i);
    }

    public void btnShowUltrasoundImage_Click(View v) { //shows ultrasound pictures
        fbdatabasepathPhotoDir = fbdatabasepathPhotoDir + "ultrasound";
        Intent i = new Intent(upload_pictures_types.this, ImageListActivity.class);//changeme
        startActivity(i);
    }

    public void btnShowFamilyImage_Click(View v) { //shows family pictures
        fbdatabasepathPhotoDir = fbdatabasepathPhotoDir + "family";
        Intent i = new Intent(upload_pictures_types.this, ImageListActivity.class);//changeme
        startActivity(i);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}



/**
 *the following script has the base assumptions:
 * 1. every user has a pre-defined tree of folders with the names:
 *      Photos
 *          - Ultrasound
 *          - Products
 *          - family
 *          - belly
 *      Documents
 *          - test results
 *          - memos
 *      Videos
 *
 *  the following code will take care of the user's will to download a file from those folders.
 *  downloads the first file from the folder.
 *
 *  StorageReference thePhoto = refToRuleEmAll.child("Photos").child("Family").child("Family.jpg");

 thePhoto.getFile(localFile)
 .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
@Override
public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
// Successfully downloaded data to local file
// ...
//Oh ya this is gut now the file haz all ze data
((ImageView)findViewById(R.id.dana_img)).setImageURI(Uri.fromFile(localFile));
}
}).addOnFailureListener(new OnFailureListener() {
@Override
public void onFailure(@NonNull Exception exception) {
// Handle failed download
// ...
}
});
 *
 */





/**

public class Download {

    FirebaseStorage storage = FirebaseStorage.getInstance();


    public Download(){

    }

    public Download(int something){

        try {
            //final File loca = File.createTempFile("a","jpg");
            final File localFile = File.createTempFile("images", "jpg");
            storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                //@Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    ImageView mmm;
                    mmm.setImageBitmap(bitmap);

                }
            });
        } catch (IOException e ) {}

    }

}

 */
