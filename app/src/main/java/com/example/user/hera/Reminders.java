package com.example.user.hera;



public class Reminders {

    int ID;
    String title,time,date;

    public Reminders(String title, String time, String date) {
        this.title = title;
        this.time = time;
        this.date = date;
    }

    public Reminders(int ID, String title, String time, String date) {
        this.ID = ID;
        this.title = title;
        this.time = time;
        this.date = date;
    }

    public Reminders() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String ReminderToString(){
        return (title + "," + time + "," + date + ","  );
    }
}
