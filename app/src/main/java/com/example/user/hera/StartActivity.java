package com.example.user.hera;

        import android.app.DatePickerDialog;
        import android.app.TimePickerDialog;
        import android.content.SharedPreferences;
        import android.os.Environment;
        import android.app.Activity;
        import android.app.SearchManager;
        import android.content.ActivityNotFoundException;
        import android.content.ContentResolver;
        import android.content.Context;
        import android.content.Intent;
        import android.database.Cursor;
        import android.graphics.PixelFormat;
        import android.media.MediaPlayer;
        import android.net.Uri;
        import android.os.Bundle;
        import android.provider.CalendarContract;
        import android.speech.RecognizerIntent;
        import android.support.annotation.NonNull;
        import android.support.design.widget.NavigationView;
        import android.support.v4.widget.DrawerLayout;
        import android.support.v7.app.ActionBarDrawerToggle;
        import android.support.v7.app.AlertDialog;
        import android.support.v7.app.AppCompatActivity;
        import android.support.v7.widget.Toolbar;
        import android.view.MenuItem;
        import android.view.View;
        import android.view.inputmethod.InputMethodManager;
        import android.widget.Button;
        import android.widget.DatePicker;
        import android.widget.ImageButton;
        import android.widget.TextView;
        import android.widget.TimePicker;
        import android.widget.Toast;
        import android.widget.VideoView;
        import android.os.AsyncTask;

        import ai.api.android.AIConfiguration;
        import ai.api.android.AIDataService;
        import ai.api.model.AIRequest;
        import ai.api.model.AIResponse;
        import ai.api.model.Result;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Bitmap;
import android.os.Environment;
import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.VideoView;
import android.os.AsyncTask;

import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;

        import java.io.BufferedReader;
        import java.io.File;
        import java.io.FileInputStream;
        import java.io.FileNotFoundException;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.io.InputStreamReader;
        import java.text.DateFormat;
        import java.text.SimpleDateFormat;
        import java.util.ArrayList;
        import java.util.Calendar;
        import java.util.Date;
        import java.util.GregorianCalendar;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;

        import ai.api.AIServiceException;

        import android.widget.EditText;

        import org.joda.time.DateTime;
        import org.joda.time.Weeks;
import ai.api.AIServiceException;

import android.widget.EditText;

import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
        import com.google.firebase.auth.FirebaseAuth;
        import com.google.firebase.auth.FirebaseUser;
        import com.google.gson.JsonElement;

        import org.joda.time.DateTime;
import org.joda.time.Weeks;


public class StartActivity extends AppCompatActivity implements
        TimePickerDialog.OnTimeSetListener,DatePickerDialog.OnDateSetListener{
    private String question;
    AIConfiguration config;
    AIDataService aiDataService;
    AIRequest aiRequest;
    VideoView m_videoView;
    private TextView txtSpeechInput;
    private Button sendButton;
    private ImageButton btnSpeak;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private EditText asrOutput;
    private Cursor cursor;
    Date currentDate;
    Date dateFromFile;
    File lastActiveDate;

    int weeksPreg;


    //===== voice reminder =======

    int day,month,year,hour,min;
    int dayFinal,monthFinal,yearFinal,hourFinal,minFinal;
    EditText dialog_time,dialog_date;
    TextView dialog_tvDate , dialog_tvTime;
    Button dialog_reminder;
    SharedPreference preference;

    //===== voice reminder =======


    //======= menu ======

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private Toolbar toolbar;

    //====== end menu =====

    private ShareDialog shareDialog;
    private int PICK_IMAGE_REQUEST=1;

    private NavigationView btnWeeklyTests;

    //search babycenter.com for the question asked (needs to override after api.ai is complete)
    protected void searchWeb(String whatToSearch) {
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        String url = "https://www.babycenter.com/search.htm?q=";

        intent.putExtra(SearchManager.QUERY, url + whatToSearch);
        startActivity(intent);
    }


    public void textInputRequest() {// this method sends String requests to API.AI
        if (question != null) {
            aiRequest.setQuery(question);//sends the requested question
            new AsyncTask<AIRequest, Void, AIResponse>() {
                @Override
                protected AIResponse doInBackground(AIRequest... requests) {
                    final AIRequest request = requests[0];
                    try {
                        final AIResponse response = aiDataService.request(request);
                        return response;
                    } catch (AIServiceException e) {
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(AIResponse aiResponse) {
                    if (aiResponse != null) {
                        // process aiResponse here
                        userInputReciever(aiResponse.getResult());//sends the result to be analyzed
                    }
                }
            }.execute(aiRequest);
        }
    }
    protected void userInputReciever(final Result apiAiResult) {


        m_videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {

                asrOutput.setText("");
                switch (apiAiResult.getAction()) {
                    case "displayPicture":
                        int check=pictureAnalyzer(apiAiResult);
                        if (check==0){
                        Intent i=new Intent(StartActivity.this,Download.class);
                        startActivity(i);}
                        break;
                    case "searchAppointment":
                        //CODE TO LAUNCH PICTURE FOR DEMO VIDEO
                        startActivity(new Intent(StartActivity.this, ReminderActivity.class));
                    case "display":
                        //a code to launch database query
                    case "SetAppointment":
                        voiceReminder();
                        break;
                    case "search": //code to launch web crawler!
                        String analizedQuestion = apiAiResult.getMetadata().getIntentName();//holds the user question as we defined it.
                        searchWeb(analizedQuestion);
                        break;
                    case "testQuery":
                        String analizedTest = ApiAiAnalyzer.testQueryAnalizer(apiAiResult);
                        searchWeb(analizedTest);
                        break;
                    case "upload":
                        startActivity(new Intent(StartActivity.this,upload_pictures_types.class));
                        break;
                    case "help":
                        break;
                    case "share":
                        sharePhotos();
                        break;

                }


                // }
            }

            private int pictureAnalyzer(Result apiAiResult) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                String fbdatabasepathPhotoDir = "Users/" + user.getUid() + "/Photos" + "/";
                Intent i;
                int check=0;
                String parameteString=null;
                final HashMap<String, JsonElement> params = apiAiResult.getParameters();
                if (params != null && !params.isEmpty()){
                for (final Map.Entry<String, JsonElement> entry : apiAiResult.getParameters().entrySet()) {
                     parameteString = entry.getValue().getAsString();}}
                switch (parameteString){
                    case "prodcut":
                        parameteString="products/";
                        fbdatabasepathPhotoDir=fbdatabasepathPhotoDir+"products/";
                        check=1;
                        i = new Intent(StartActivity.this, ImageListActivity.class);
                        startActivity(i);
                        break;
                    case "family":
                        check=1;
                        parameteString="bellyProgress/";
                        fbdatabasepathPhotoDir=fbdatabasepathPhotoDir+"family/";
                         i = new Intent(StartActivity.this, ImageListActivity.class);
                        startActivity(i);

                        break;
                    case "belly":
                        check=1;
                        parameteString="bellyProgress/";
                        fbdatabasepathPhotoDir=fbdatabasepathPhotoDir+"belly/";
                         i = new Intent(StartActivity.this, ImageListActivity.class);
                        startActivity(i);
                        break;
                    case "ultrasound":
                        check=1;
                        parameteString="ultrasound/";

                        fbdatabasepathPhotoDir=fbdatabasepathPhotoDir+"ultrasound/";
                         i = new Intent(StartActivity.this, ImageListActivity.class);
                        startActivity(i);
                        break;

                }

                return check;
            }
        });

        //GUY ! GIVE videoName THE STRING WHICH CONTAINS THE NAME OF THE VIDEO TO BE PLAYED !
        String videoName;
        if (!(new String("videoResponse").equals(apiAiResult.getAction()))) {
            videoName = apiAiResult.getFulfillment().getSpeech();//gets video name
        } else {
            videoName = new ApiAiAnalyzer().videoNameFinder(apiAiResult);
        }
        String uriPath = "android.resource://" + getPackageName() + "/" + getResources().getIdentifier(videoName, "raw", getPackageName());
        Uri uri2 = Uri.parse(uriPath);
        m_videoView.setVideoURI(uri2);
        m_videoView.requestFocus();
        m_videoView.start();
    }

    ;


    void PlayVideo(final String i_videoName) {

        m_videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                PlayVideo("s1");

            }
        });

        String uriPath = "android.resource://" + getPackageName() + "/" + getResources().getIdentifier(i_videoName, "raw", getPackageName());
        Uri uri2 = Uri.parse(uriPath);
        m_videoView.setVideoURI(uri2);
        m_videoView.requestFocus();
        m_videoView.start();
    }

    ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        shareDialog = new ShareDialog(this);

        preference = new SharedPreference() ;

        //======== menu ========
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        toolbar = (Toolbar) findViewById(R.id.nav_action_bar);
        setSupportActionBar(toolbar);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.navView);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case(R.id.my_hera): {
                        startActivity(new Intent(StartActivity.this, StartActivity.class));
                        break;
                    }
                    case(R.id.weekly_tests): {
                        startActivity(new Intent(StartActivity.this, WeeklyTests.class));
                        break;
                    }
                    case(R.id.download_images): {
                        startActivity(new Intent(StartActivity.this, Download.class));
                        break;
                    }
                    case(R.id.forbidden_food): {
                        startActivity(new Intent(StartActivity.this, forbiddenFood.class));
                        break;
                    }
                    case(R.id.menu_reminder): {
                        startActivity(new Intent(StartActivity.this, ReminderActivity.class));
                        break;
                    }
                    case (R.id.upload_pics):
                        startActivity(new Intent(StartActivity.this, upload_pictures_types.class));
                        break;
                }
                return true;

            }



        });
        //======= end menu =========

        //setSupportActionBar(mToggle);

//        googleCalendarApi=new GoogleCalendarApi();
        getWindow().setFormat(PixelFormat.UNKNOWN);
        config = new AIConfiguration("d7aee4d5676d4bb0ac4eb69f19458c1b",
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);
        aiDataService = new AIDataService(this, config);
        aiRequest = new AIRequest();
        m_videoView = (VideoView) findViewById(R.id.VideoView1);
        asrOutput = (EditText) findViewById(R.id.asrOutput);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");




        String lastActiveDatePath=Environment.getExternalStorageDirectory().getAbsolutePath()+"/Hera";
        SharedPreferences sharedPref= getSharedPreferences("lastActivateInfo",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPref.edit();
        String dateFromFile=sharedPref.getString("Date","FirstUse");
        Date lastActivateDate;
        if (dateFromFile.equals("FirstUse")){
                long milisecs=1500584400000L;
                 lastActivateDate=new Date(milisecs);
                editor.putString("Date",lastActivateDate.toString());
            editor.commit();
        }
        else{
//            asrOutput.setText("This is the else statement");
             lastActivateDate=new Date(dateFromFile);
        }
        currentDate=new Date();
        editor.putString("Date",currentDate.toString());
        editor.commit();
        DateTime oldDate=new DateTime(lastActivateDate);
        DateTime todaysDate=new DateTime(currentDate);
        if (Weeks.weeksBetween(oldDate,todaysDate).getWeeks()>=1) {
            weeksPreg = weeksPreg();
            switch (weeksPreg) {
                case 8:
                    PlayVideo("ultrasound_test");
                    break;
                case 12:
                    PlayVideo("first_trimester");
                    break;
                case 16:
                    PlayVideo("ultrasound_test");
                    break;
                case 17:
                    PlayVideo("second_trimester");
                    break;
                case 23:
                    PlayVideo("ultrasound");
                    break;
                case 26:
                    PlayVideo("blood_count_test");
                    break;
                case 28:
                    PlayVideo("blood_count_results");
                    break;
                case 1:PlayVideo("s1");break;
                case 2:PlayVideo("s1");break;
                case 3:PlayVideo("s1");break;
                case 4:PlayVideo("s1");break;
                case 5:PlayVideo("s1");break;
                case 6:PlayVideo("s1");break;
                case 7:PlayVideo("s1");break;
                case 9:PlayVideo("s1");;break;
                case 10:PlayVideo("s1");break;
                case 11:PlayVideo("s1");break;
                case 13:PlayVideo("s1");break;
                case 14:PlayVideo("s1");break;
                case 15:PlayVideo("s1");break;
                case 18:PlayVideo("s1");break;
                case 19:PlayVideo("s1");break;
                case 20:PlayVideo("s1");break;
                case 21:PlayVideo("s1");break;
                case 22:PlayVideo("s1");break;
                case 24:PlayVideo("s1");break;
                case 25:PlayVideo("s1");break;
                case 27:PlayVideo("s1");break;
                case 29:PlayVideo("s1");break;
                case 30:PlayVideo("s1");break;
                case 31:PlayVideo("s1");break;
                case 32:PlayVideo("s1");;break;
                case 33:PlayVideo("s1");break;
                case 34:PlayVideo("s1");break;
                case 35:PlayVideo("s1");break;
                case 36:PlayVideo("s1");break;
                case 37:PlayVideo("s1");break;
                case 38:PlayVideo("s1");break;
                case 39:PlayVideo("s1");break;
                case 40:PlayVideo("s1");break;
                case 41:PlayVideo("s1");break;
                case 42:PlayVideo("s1");break;
            }
        }
        //first interaction
        else
            PlayVideo ("s1");
        txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);
        btnSpeak = (ImageButton) findViewById(R.id.btnSpeak);
        sendButton = (Button) findViewById(R.id.sendButton);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do something in response to button
                m_videoView.stopPlayback();
                ((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE))
                        .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
                EditText editText = (EditText) findViewById(R.id.editText);
                question =  (editText.getText().toString());
                hideSoftKeyboard(StartActivity.this);
                textInputRequest();


               //Download download=new Download();
               // download.userCommand("ultrasound/");

                //voiceReminder();
                //textInputRequest();
                //sharePhotos();

            }
        });
        // hide the action bar
        btnSpeak.setOnClickListener(new View.OnClickListener() {

            //asr button
            @Override
            public void onClick(View v) {
                m_videoView.stopPlayback();
                promptSpeechInput();
            }
        });
        m_videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(StartActivity.this, "Error with response, bug reported",
                        Toast.LENGTH_SHORT).show();
                PlayVideo("s1");
                return true;
            }
        });



    }
    //when returning to this activity from a different activity
    @Override
    protected void onRestart() {
        super.onRestart();
        PlayVideo("s1");
        m_videoView = (VideoView) findViewById(R.id.VideoView1);
        txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);
        btnSpeak = (ImageButton) findViewById(R.id.btnSpeak);
        // hide the action bar
        btnSpeak.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do something in response to button
                EditText editText = (EditText) findViewById(R.id.editText);
                question =  (editText.getText().toString());
                textInputRequest();
            }
        });
    }

    /**
     * Showing google speech input dialog
     * */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txtSpeechInput.setText(result.get(0));
                    question = (String) result.get(0);
                    asrOutput.setText(question);

                }
                textInputRequest();
                break;
            }

        }
        if (requestCode==PICK_IMAGE_REQUEST && data!=null && data.getData()!=null) {

            Bitmap image = null;
            try {
                image = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(image)
                    .build();

            if (ShareDialog.canShow(SharePhotoContent.class)) {
                SharePhotoContent sharePhotoContent = new SharePhotoContent.Builder()
                        .addPhoto(photo)
                        .build();

                shareDialog.show(sharePhotoContent);
            }
        }
    }


   // @Override
    //public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    // getMenuInflater().inflate(R.menu.activity_main2, menu);
    //   return true;
    // }
    private static void addToCalendar(Context ctx, final String title, final long dtstart, final long dtend) {
        final ContentResolver cr = ctx.getContentResolver();
        final String CALENDAR_LOCATION="content://com.android.calendar/calendars";
        final Uri CALENDAR_URI=Uri.parse(CALENDAR_LOCATION);
        Cursor cursor ;

    }
    public static void Save(File file, String data) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
        }
        catch (FileNotFoundException e) {e.printStackTrace();}
        try {
            try {
                fos.write(data.getBytes());
            }
            catch (IOException e) {e.printStackTrace();}
        }
        finally {
            try {
                fos.close();
            }
            catch (IOException e) {e.printStackTrace();}
        }
    }
    public static String[] Load(File file)
    {
        FileInputStream fis = null;
        try
        {
            fis = new FileInputStream(file);
        }
        catch (FileNotFoundException e) {e.printStackTrace();}
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);

        String test;
        int anzahl=0;
        try
        {
            while ((test=br.readLine()) != null)
            {
                anzahl++;
            }
        }
        catch (IOException e) {e.printStackTrace();}

        try
        {
            fis.getChannel().position(0);
        }
        catch (IOException e) {e.printStackTrace();}

        String[] array = new String[anzahl];

        String line;
        int i = 0;
        try
        {
            while((line=br.readLine())!=null)
            {
                array[i] = line;
                i++;
            }
        }
        catch (IOException e) {e.printStackTrace();}
        return array;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void voiceReminder(){

        AlertDialog.Builder mBuilder= new AlertDialog.Builder(StartActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.dialog,null);
        dialog_time = (EditText) mView.findViewById(R.id.dialog_time);
        dialog_date = (EditText) mView.findViewById(R.id.dialog_date);
        dialog_reminder = (Button) mView.findViewById(R.id.btn_reminder);

        dialog_tvDate = (TextView) mView.findViewById(R.id.dialog_tvDate);
        dialog_tvTime = (TextView) mView.findViewById(R.id.dialog_tvTime);

        dialog_tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(StartActivity.this,StartActivity.this,year,month,day);
                datePickerDialog.show();
            }
        });

        dialog_tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                hour = c.get(Calendar.HOUR_OF_DAY);
                min = c.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(StartActivity.this,StartActivity.this,
                        hour,min, android.text.format.DateFormat.is24HourFormat(StartActivity.this));
                timePickerDialog.show();
            }
        });



        dialog_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent calIntent = new Intent(Intent.ACTION_INSERT);
                calIntent.setType("vnd.android.cursor.item/event");
                calIntent.putExtra(CalendarContract.Events.TITLE, "doctor's appointment");

                GregorianCalendar ScalDate = new GregorianCalendar(yearFinal, monthFinal, dayFinal,hourFinal,minFinal);
                GregorianCalendar EcalDate = new GregorianCalendar(yearFinal, monthFinal, dayFinal,hourFinal,minFinal);
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                        ScalDate.getTimeInMillis());
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                        EcalDate.getTimeInMillis());
                String date = Integer.toString(dayFinal) + "." + Integer.toString(monthFinal) + "." + Integer.toString(yearFinal);
                String time = Integer.toString(hourFinal) + ":" + Integer.toString(minFinal);

                // Creating Reminder

                Reminders reminders = new Reminders("doctor's appointment",time,date);

                preference.addFavorite(StartActivity.this,reminders);

                startActivity(calIntent);
            }
        });

        mBuilder.setView(mView);
        AlertDialog dialog = mBuilder.create();
        dialog.show();
    }


    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        yearFinal = i;
        monthFinal = i1;
        dayFinal = i2;

        dialog_date.setText(dayFinal + "." + monthFinal + "." + yearFinal);
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {

        hourFinal = i;
        minFinal = i1;

        dialog_time.setText(hourFinal + ":" + minFinal);

    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);}

    public void getLastReminder(){

        List<Reminders> reList = new ArrayList<>();
        reList = preference.loadFavorites(StartActivity.this);


        AlertDialog.Builder mBuilder= new AlertDialog.Builder(StartActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_last_reminder,null);


        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        TextView tv_Last = (TextView) mView.findViewById(R.id.tv_Last_Reminder);
        TextView tv_Last_Time = (TextView) mView.findViewById(R.id.tv_Last_Time);
        TextView tv_Last_Date = (TextView) mView.findViewById(R.id.tv_Last_Date);
        Button btn_Last_reminder = (Button) mView.findViewById(R.id.btn_Last_reminder);



        if(reList.isEmpty()){
            tv_Last_Date.setText("you dont have any appointment scheduled");
            tv_Last_Date.setTextSize(17);
            tv_Last.setVisibility(View.INVISIBLE);
            tv_Last_Time.setVisibility(View.INVISIBLE);
        }else{
            tv_Last.setText("your next appointment at:");
            tv_Last_Date.setText(reList.get(reList.size()-1).getDate());
            tv_Last_Time.setText(reList.get(reList.size()-1).getTime());
        }

        btn_Last_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.show();


    }

    public void sharePhotos() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    public int weeksPreg(){
        SharedPreferences sharedPref= getSharedPreferences("signUpInfo",MODE_PRIVATE);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String pregWeek=sharedPref.getString("pregWeek","");
        String signDate=sharedPref.getString("signDate","");
        int pregWeekDate=Integer.parseInt(pregWeek);
        Date signDateDate=new Date(signDate);
        Date currentDate=new Date();
        DateTime d1=new DateTime(signDateDate);
        DateTime d2=new DateTime(currentDate);
        return Weeks.weeksBetween(d1,d2).getWeeks()+pregWeekDate;
    }
}
